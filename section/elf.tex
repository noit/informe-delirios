\begin{section}{Loader}

\begin{subsection}{Introducción}

El objetivo para el loader de binarios es soportar la carga estática de binarios en formato ELF64. Cabe destacar que la elección de ELF64 como formato predilecto está basada fundamentalmente en el amplio soporte que tiene. Otra alternativa es inventar un formato propio donde se podría, por ejemplo, especificar más de un punto de entrada al programa para que cada procesador lógico ya se encuentre en su sección de código que le corresponde. Sin embargo, las ventajas que esto pueda ofrecer no son una prioridad obvia. De hecho, existen más herramientas adaptadas al trabajo con modelo de \textit{threads} que el de programas separados para cada procesador.

\end{subsection}

\begin{subsection}{Características soportadas}

Se puede cargar un ejecutable estático desde un archivo (el cual se lee mediante la interfaz de \textit{input/output}). Al finalizar la ejecución de la aplicación mediante la syscall \textbf{exit()}, se restaura el contexto del shell para poder continuar con otras tareas.

Los ejecutables deben ser linkeados con un linker script que los ubique en un área de memoria libre de DeliriOS. Actualmente no existen mecanismos que protejan la memoria del kernel así que es responsabilidad del desarrollador de aplicaciones linkear correctamente para poder restaurar el contexto y asegurar el funcionamiento correcto de las syscalls. En el repositorio se encuentran algunas aplicaciones de ejemplo donde se linkea a la dirección donde comienza el espacio reservado para las aplicaciones, pero se podría linkear la dirección base en cualquier otro punto de este espacio también.

\end{subsection}

\pagebreak

\begin{subsection}{Implementación}

El loader está implementado para leer un archivo del sistema de archivos virtual, cargarlo y ejecutarlo si es un binario válido.

\begin{subsubsection}{Carga}

\begin{center}
 \includegraphics[scale=0.5]{images/loader.pdf} \\
 \textit{Segmentos de un binario ELF y su relación con la memoria}
\end{center}

Lo primero que hace el loader es leer el encabezado del archivo. Carga los primeros bytes en memoria y los interpreta según esta estructura definida por el formato ELF64:

\input{code/elf_header.tex}

\pagebreak

\begin{description}
\item[magic] El lugar donde se encuentra el número mágico de ELF. Si los primeros cuatro bytes no son 0x7f'ELF', rechazamos el archivo como inválido. También revisamos otros bytes para verificar que se trata de un archivo ELF64 y que sus estructuras de datos están organizadas como little endian.
\item[type] Indica si es un objeto reubicable, cargable o compartido entre otras posibilidades. Nosotros soportamos únicamente los cargables. 
\item[version] Indica la versión del formato ELF64. Actualmente tiene un único valor porque no se hicieron revisiones aún.
\item[entry\_point] La dirección virtual donde se debe comenzar la ejecución en un archivo ejecutable. Si no se trata de un ejecutable, siempre es cero.
\item[ph\_offset] Es el offset del archivo en bytes donde se encuentra la \textit{program header table}. Esta está presente en todo ejecutable y en ella están descriptos los segmentos a cargar.
\item[sh\_offset] Es el offset del archivo en bytes donde se encuentra la \textit{section header table}. Esta está presente en todo objeto compartido \footnote{El cual contiene código independiente de su posición en memoria en tiempo de ejecución.} y/o reubicable \footnote{El cual contiene código dependiente de su posición en memoria que aún tiene etiquetas indefinidas. Estas deben ser resueltas (linkeadas o enlazadas) por el loader para el correcto funcionamiento del objeto.}. Como no ofrecemos soporte para estas características, ignoramos esta tabla.
\item[flags] Se trata de flags característicos para cada arquitectura de procesador. Actualmente tienen que ver con los procesadores que soportan tanto orden \textit{little endian} como \textit{big endian}. Aquí se informa al loader cuál de los dos modos espera el archivo objeto. En nuestro caso lo ignoramos ya que solo soportamos arquitectura AMD64.
\item[header\_size] El tamaño en bytes de este header. Actualmente lo ignoramos pero puede servir para revisiones futuras del formato donde se agreguen campos al encabezado.
\item[ph\_entry\_size] El tamaño en bytes de cada elemento en la \textit{program header table}.
\item[ph\_entry\_count] La cantidad de elementos en la \textit{program header table}.
\item[sh\_entry\_size] El tamaño en bytes de cada elemento en la \textit{section header table}.
\item[sh\_entry\_count] La cantidad de elementos en la \textit{section header table}.
\item[sh\_string\_table\_index] Es el índice de la \textit{section header table} de la sección que contiene la tabla de nombres en strings para todas las secciones.
\end{description}

Al momento de cargar en memoria el binario es necesario buscar la \textit{program header table} e interpretar cada entrada en ella:

\input{code/elf_program_header.tex}

\begin{description}
\item[type] Este atributo identifica el tipo de segmento descripto. El loader busca únicamente aquellos que son para cargar en memoria tal como están. El valor en cuestión está definido como \textbf{ELF\_LOAD} aunque en el estándar se puede encontrar como \textbf{PT\_LOAD}.
\item[flags] Describe atributos del segmento. Habitualmente se indica aquí que la memoria que albergue los datos del segmento debe ser de solo lectura o permitir la ejecución de su contenido. También tiene 16 bits reservados en la parte superior para uso del sistema operativo (ocho de ellos son características dadas de acuerdo al procesador). Como DeliriOS no provee tipo alguno de protección, ignoramos este campo.
\item[offset] Especifica el offset, en bytes, desde el comienzo del archivo donde se encuentra el segmento.
\item[virtual\_address] Contiene la dirección de memoria virtual del segmento.
\item[physical\_address] Este atributo está reservado para sistemas con direccionamiento a memoria física.
\item[file\_size] Contiene el tamaño en bytes de la imagen en archivo del segmento.
\item[memory\_size] Contiene el tamaño en bytes de la imagen en memoria del segmento.
\item[align] Especifica el alineamiento requerido. \textbf{virtual\_address} y \textbf{offset} deben ser congruentes módulo \textbf{align}.
\end{description}

Para cargar en memoria lo que se hace es, una vez encontrado un \textit{program header entry} en esta tabla que sea cargable, leer la información básica del segmento (dirección virtual, offset de la imagen en el archivo) y realizar el copiado de la imagen del segmento en el archivo a memoria. Cada segmento cargable puede tener un tamaño en memoria más grande que el de la imagen en el binario. El loader se encarga de inicializar esta memoria sobrante a cero tal como indica el estándar ELF.

Al terminar la carga se escribe en la siguiente estructura el \textbf{entry\_point} y el \textbf{loader\_error}:

\input{code/app_status.tex}

\begin{description}
\item[entry\_point] La dirección virtual donde debe comenzar la ejecución el loader. La copiamos tal como está del encabezado ELF64.
\item[running\_status] Este flag indica si hay una aplicación ejecutándose. Si el loader no cargó una aplicación o si se salió de una aplicación este flag es cero. En caso contrario es uno. Lo utilizamos para hacer un spinlock sobre los procesadores lógicos que intentaron terminar la aplicación justo cuando otro procesador ya estaba iniciando el protocolo de terminación.
\item[stack\_pointer] Aquí guardamos el puntero del stack antes de saltar al punto de entrada de la aplicación. Con esto podemos restaurar el contexto del kernel previo al lanzamiento de la aplicación.
\item[exit\_code] Aquí guardamos el valor recibido por parámetro en la syscall \textbf{exit()}. La idea es utilizarlo como un código de terminación para proveer feedback al que lanza la aplicación.
\item[exit\_core\_id] Aquí guardamos el identificador del procesador que llamó a la syscall \textbf{exit()}. Si bien es posible obtener esta información realizando acrobacias con el código de terminación, preferimos facilitar esta información dado que es excesivamente fácil de obtener durante la ejecución de la syscall.
\item[loader\_error] Cuando falla el loader en la fase de carga se guarda el número de error aquí para poder imprimir la información pertinente en el shell. Si no falla el loader, se le asigna cero.
\end{description}

Cuando se llega al final de la tabla sin haber encontrado ningún error, el loader finaliza la carga y pasa a ejecutar la aplicación.

\end{subsubsection}

\begin{subsubsection}{Ejecución}

Con esto, solo queda transicionar y ejecutar la aplicación en sí. Para hacer esto se guardan los registros del procesador para cumplir con la convención C para AMD64 en la pila. Luego se guarda el puntero del stack en \textbf{stack\_pointer} de \textbf{application\_data} y se asigna un uno a \textbf{running\_status} lo que indica que está corriendo una aplicación en el sistema. A continuación se realiza el salto al punto de entrada. Todo esto es algo que ocurre en el BSP ya que es el que se encarga de ejecutar el shell.

En los APs lo que ocurre es que se encuentran esperando a que \textbf{running\_status} sea uno. Cuando esto se cumple, saltan a \textbf{entry\_point}. De esta forma todos los procesadores se encuentran ejecutando la aplicación. En el caso de los APs no resulta necesario guardar el puntero a la pila ya que se encuentra vacía en el momento en que se encuentran esperando. Por lo tanto es solo cuestión de reinicializar el puntero como si se iniciara el procesador por primera vez.

\end{subsubsection}

\begin{subsubsection}{Terminación}

La terminación está soportada mediante una syscall. Terminar la aplicación no implica una garantía del correcto funcionamiento del kernel ya que la aplicación corre en privilegio 0. Sin embargo, mientras no se escriba en lugares sensibles de la memoria del kernel o se modifiquen registros de control del procesador, se podrá restablecer el contexto de ejecución del shell.

Cuando un thread\footnote{Aquí, por la naturaleza de DeliriOS, un thread equivale a un procesador lógico en tiempo completo.} llama a la syscall exit, lo primero que se hace es adquirir un lock sobre la terminación de la aplicación. Esto se realiza con un exchange atómico sobre el flag que indica el estado del sistema en \textbf{application\_data}. Si no se adquirió con éxito el lock, se para el procesador para esperar a que llegue la señal de terminación de otro thread. En caso contrario, se guarda el identificador del procesador lógico que está terminando la aplicación en \textbf{exit\_core\_id} y el código de error recibido en \textbf{exit\_code} en la estructura \textbf{application\_data}. Luego, se envía una interrupción a todos los procesadores incluyendo al actual.

La rutina de atención de esta interrupción se encarga de restaurar el contexto del shell. En el caso de los APs, esto significa colocarlos en espera reinicializando el puntero del stack. En el caso del BSP, se resume la ejecución luego del salto al punto de entrada con el mismo puntero del stack que se tenía antes de lanzar la aplicación y se restauran los registros del procesador guardados en el stack para cumplir con la convención C para AMD64.

\end{subsubsection}

\end{subsection}

\end{section}
