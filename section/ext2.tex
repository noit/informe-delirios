\begin{section}{Ext2}

\begin{subsection}{Por qué EXT2}

Había varios filesystems para elegir. JuampiOS implementa el de MINIX que es a decir verdad un precursor de EXT2. El problema con el filesystem de MINIX es que la documentación y el soporte es inexistente, además de que es un filesystem más limitado. Luego había otras opciones, como FAT o ISO9660, pero las extensiones de FAT están patentadas por Microsoft y es muy limitado e ISO9660 si bien era una buena opción ya que es el filesystem estándar de los CDs, no es booteable por GRUB y tiene poco soporte fuera de los CDs. \par
En contraste, EXT2 es el filesystem por defecto de Linux, diseñado específicamente para él, y tiene soporte a nivel global tanto por Bootloaders como Sistemas Operativos. EXT2 si bien no tiene una especificación documentada más allá del source code de Linux, tiene documentación no oficial bastante buena en especial una realizada por Dave Poirier\footnote{\url{http://www.nongnu.org/ext2-doc/ext2.html}} que es la más completa y explicativa. \par
Además, EXT2 tiene múltiples features que pueden ser soportadas a futuro si es necesario, y fue incrementalmente mejorado en EXT3 y EXT4, si es que se llegan a necesitar incluso más features.

\end{subsection}

\begin{subsection}{Estructura}

EXT2 está basado en el filesystem clásico de Unix. A grandes rasgos:
\begin{itemize}
\item Se divide el espacio de memoria a utilizar en \textbf{bloques} contiguos de igual tamaño, indizados por números enteros de 32 bits a partir del 0.
\item Los bloques se agrupan a su vez en \textbf{grupos de bloques} contiguos de igual tamaño.
\item Los archivos son representados por el concepto de \textbf{inodos} que son estructuras dentro de una tabla en cada grupo, estos apuntan a una determinada cantidad de bloques utilizando sus índices que forman el contenido del mismo. Notar que esto incluye a los directorios, que son un tipo especial de archivo.
\item La existencia del \textbf{superbloque}, que está en una posición fija dentro del filesystem y contiene los datos más generales del mismo, el cual es utilizado como punto de entrada para detectar su contenido.
\item Luego del superbloque se encuentra una tabla con descriptores de grupos de bloque, con datos necesarios para poder leer cada grupo.
\end{itemize}

Existen dos versiones de EXT2 bien marcadas, la versión \textit{major} 0 y la \textit{major} 1. La actual y más usada de la que vamos a hablar es la versión 1, donde se agregó un poco de flexibilidad y más parámetros al superbloque e inodos.

\begin{subsubsection}{Tamaños}

Para ofrecer una visión más precisa, el tamaño de cada elemento en EXT2 está dado por:

\begin{description}
\item[Superbloque] Siempre 1KiB
\item[Bloque] Parametrizable, está denotado por la fórmula $1KiB * 2^n$, siendo n un número mayor o igual a cero que se almacenará en el superbloque para poder parsear correctamente.
\item[Grupo] Depende del tamaño del bloque. Como cada grupo contiene un bloque que define un mapa de bits indicando el estado de sus bloques (Libre u Ocupado), los grupos miden como mucho \textit{[Tamaño de un bloque en bits]} bloques. Por ejemplo, para bloques de 2KiB, miden como mucho 16Ki bloques, este valor está ubicado en el superbloque.
\item[Inodo] Originalmente siempre 128 bytes. Se hizo variable en la versión 1, en ese caso el tamaño en bytes está ubicado en el superbloque y debe ser una potencia de 2 mayor a 128 y menor al tamaño de bloque.
\item[Descriptor de grupo] Esto es, una entrada de la tabla de grupos, siempre 32 bytes.
\end{description}

\end{subsubsection}

\pagebreak

\begin{subsubsection}{Estructura general}

Un ejemplo de estructura general de EXT2 es el siguiente:

\begin{center}
 \includegraphics[scale=0.5]{images/ext2_general.pdf} \\
 \textit{Estructura general de EXT2 con bloques de 2KiB}
\end{center}

\begin{itemize}
\item El primer KiB está siempre reservado para los sectores de booteo. 
\item El segundo KiB contiene siempre al superbloque que mide exactamente 1KiB.
\item A partir del bloque siguiente a donde se encuentre el superbloque\footnote{Como el tamaño de bloque es $1KiB * 2^n$ para algún $n$, para bloques de 1KiB el superbloque estará en el bloque 1 y para cualquier otro tamaño, en el bloque 0 como en el ejemplo de la imagen. Esto es solo un caso borde que hay que tener en cuenta. \label{note1}} se encontrará la tabla de grupos de bloque, la cual puede ocupar varios bloques.
\item El grupo 0 comienza contando desde el bloque donde se encuentre el superbloque\footref{note1}.
\end{itemize} 

A su vez, cada grupo contiene lo siguiente:
\begin{itemize}
\item Un bitmap de bloques, esto es una serie de bits que indican qué bloques tiene libres (Bit en 0) y qué bloques tiene ocupados (Bit en 1), debe entrar en un bloque.
\item Un bitmap de inodos, esto es una serie de bits que indican qué inodos tiene libres (Bit en 0) y qué inodos tiene ocupados (Bit en 1), debe entrar en un bloque.
\item Una tabla de inodos, la cantidad de inodos por grupo (Y por lo tanto, por tabla) está definida en el superbloque. La cantidad de inodos está delimitada por el bitmap de inodos y debe ser múltiplo de la cantidad de inodos que entran en un bloque. Todos los inodos del filesystem están indizados absolutamente desde 1, esto es, si un grupo contiene 2048 inodos, el grupo 0 contendrá los inodos 1 a 2048, el grupo 1 los inodos 2049 a 4096, etc.
\end{itemize}

Utilizando la tabla de descriptores de grupo, obtenemos datos del contenido de cada grupo, esto es en orden:
\begin{itemize}
\item El número de bloque absoluto del bitmap de bloques.
\item El número de bloque absoluto del bitmap de inodos.
\item El número de bloque absoluto del primer bloque de la tabla de inodos.
\item La cantidad de bloques libres
\item La cantidad de inodos libres
\item La cantidad de inodos que son directorios
\end{itemize}

Los bloques y los inodos son siempre indizados globalmente, la división en grupos no cambia este hecho. Solo falta entender el concepto de inodos y cómo se relacionan con los bloques para poder ser capaces de leer correctamente el filesystem.

\end{subsubsection}

\begin{subsubsection}{Inodos}

Como se ha dicho anteriormente, cada inodo representa un archivo o directorio. Los inodos contienen metadatos relevantes del archivo, a destacar:

\begin{itemize}
\item Los permisos de acceso. Incluyendo el clásico número octal de 3 cifras más otros 3 bits: Sticky bit que vale más que nada para forzar privilegios en los archivos de un directorio y los set process User/Group ID que setea los privilegios de Usuario/Grupo del archivo respectivamente al proceso si se ejecutase el archivo.
\item El tipo de archivo. Puede ser: Archivo común, directorio, block device, character device, socket, fifo o symlink.
\item El tamaño en bytes del archivo, vale para todos los tipos incluyendo directorios, más adelante se hablará de esto.
\item Los tiempos de acceso, borrado, creación, etc.
\end{itemize}

Por último lo más importante: contiene los números de los bloques que una vez concatenados formarían el archivo completo. Un lector atento se dará cuenta de que debido al tamaño fijo de los inodos, no sería posible representar archivos de gran tamaño ya que esto supondría almacenar muchos números de bloque, podemos despejar dicha inquietud con la siguiente imagen:

\begin{center}
 \includegraphics[scale=0.5]{images/inodo.pdf} \\
 \textit{Estructura de un archivo junto con el inodo que lo representa} \\
 \textit{Donde N es la cantidad de números de bloque que entran en un bloque} \\
 \textit{Esto es N = block\_size / 4, ya que un número de bloque son 4 bytes}
\end{center}

\pagebreak

Hay dos tipos distintos de bloques, estos son:

\begin{description}
\item[De datos] Estos bloques forman el contenido del archivo. Se los identifica por su número absoluto en el filesystem como dijimos antes, en el inodo hay exactamente 12 de estos números (en orden) que apuntan a 12 bloques de datos, se los llama punteros a bloque directos. Si el archivo es lo suficientemente chico podrían no usarse los 12 punteros, en cuyo caso el resto de los números son inválidos. Los números que se les otorgaron en la imagen son relativos al archivo partiendo desde el 0, para poder comprender las distancias entre cada uno de los bloques mostrados y la magnitud que puede llegar a tener un archivo, no confundir con el número absoluto que los representa en el filesystem. Este número otorgado va a ser importante para entender la implementación más adelante.
\item[Indirecto] Estos bloques contienen punteros a otros bloques, esto es un arreglo de números de bloque que apuntan a otros bloques. Como dice la imagen, la cantidad de números de bloque que contienen es $block\_size / 4$, ya que un número de bloque son 4 bytes. Podrían contener menos si el archivo no es lo suficientemente grande, en cuyo caso el resto de los valores son inválidos.
\end{description}

Y a su vez hay 3 tipos de bloques indirectos, en tres niveles de profundidad:

\begin{description}
\item[Simple] Contiene punteros a bloque directos. El inodo contiene un número de bloque apuntando a uno de estos (bn\_single en la imagen), si es que el tamaño del archivo es suficiente, el cual se lo denomina puntero a bloque simple.
\item[Doble] Contiene punteros a bloques simples, el inodo contiene un número de bloque apuntando a uno de estos (bn\_double en la imagen) si es que el tamaño del archivo es suficiente, el cual se lo denomina puntero a bloque doble.
\item[Triple] Contiene punteros a bloques dobles. El inodo contiene un número de bloque apuntando a uno de estos (bn\_triple) en la imagen, si es que el tamaño del archivo es suficiente, el cual se lo denomina puntero a bloque triple. Como es el nivel más grande posible de bloque indirecto solo puede haber uno por archivo, que es el apuntado por el inodo.
\end{description}

Gracias a esto, un inodo puede apuntar a una inmensa cantidad de bloques utilizando dichos bloques indirectos. La cantidad máxima de bloques que puede tener un archivo está delimitada por el tamaño de bloque, debido que a su vez esto delimita la cantidad de números de bloque dada por $N = block\_size / 4$. En total nos quedan $12 + N + N^2 + N^3$ bloques de datos\footnote{Si tomamos los 12 bloques apuntados por el inodo, más los N bloques apuntados por el bloque indirecto simple, más los $N^2$ bloques apuntados por los $N$ bloques simples apuntados por el bloque indirecto doble, más los $N^3$ bloques apuntados por los $N^2$ bloques simples apuntados por los $N$ bloques dobles apuntados por el bloque indirecto triple}. Para bloques de 1KiB, esto es 16GiB de tamaño máximo de archivo, sin contar el costo de los bloques indirectos. \par

\begin{paragraph}{Directorios}

Lo único importante que nos falta para entender cómo leer archivos de EXT2 son los directorios. En EXT2, estos son simplemente un tipo especial de archivo. Esto es, están representados por un inodo al igual que un archivo. Se los diferencia con el campo de tipo en el inodo como vimos anteriormente, y poseen una estructura particular dentro del contenido del archivo que define el contenido del directorio. \par
Notar que es aquí donde se almacenan los nombres de los archivos dentro del directorio y no en el inodo del archivo. Basta con conocer el inodo del directorio raíz, para poder comenzar a recorrer el filesystem. Alegrará saber que dicho inodo está reservado y es siempre el número 2. \par
Solo falta entender la estructura interna del directorio. Está formado por entradas de directorio una seguida de la otra, de tamaño variable. A su vez cada entrada tiene los siguientes campos:

\begin{description}
    \item[Inodo] El número de inodo del archivo al que estamos apuntando en esta entrada. Si es 0, la entrada se considera nula, esto es libre para ser usada en caso de tener que escribir una entrada o libre de ser ignorada en caso de tener que avanzar.
    \item[Tamaño de la entrada] El tamaño total de la entrada, contando todos los campos incluyendo el nombre y el padding necesario hasta la próxima entrada. Debe ser múltiplo de 4. Necesario para saber dónde está ubicada la siguiente entrada. Para decirlo de otro modo, la suma de estos campos de todas las entradas debería dar el tamaño del bloque.
    \item[Tipo de archivo] Esto originalmente era la parte alta del tamaño del nombre que es el siguiente campo, pero si se activa un flag en el superbloque (Que en general es activado de facto) se toma como tipo de la entrada. Notar que no son los mismos valores que en POSIX, sino un número del 0 al 7, ver tabla más adelante.
    \item[Tamaño del nombre] El tamaño del nombre que comienza justo después de este campo. Debido a que el otro campo es generalmente usado para el tipo nos da un tamaño de nombre máximo por archivo de 255 caracteres.
    \item[Nombre] El nombre del archivo, \textbf{no necesariamente un null terminated string}, hay que utilizar el tamaño del nombre para poder leerlo con seguridad.
    \item[Padding] Luego del nombre viene el suficiente padding para completar el tamaño de la entrada.
\end{description}

Los valores para los tipos son: \par

\begin{tabular}{r|l}
Valor & Tipo \\ \hline
0 & Indefinido/NULL \\ 
1 & Archivo común \\
2 & Directorio \\
3 & Char Device \\
5 & Block Device \\
4 & FIFO \\
6 & Socket \\
7 & Symbolic Link \\
\end{tabular} \\

Podemos ver un ejemplo de esto en la siguiente imagen:

\begin{center}
 \includegraphics[scale=0.5]{images/directorio.pdf} \\
 \textit{Estructura de un directorio raíz con bloques de 1KiB}
\end{center}

Es un directorio raíz (Inodo 2), que sucesivamente, contiene las siguientes entradas:

\begin{description}
\item[.] Esta es una entrada estándar de tipo directorio (2) que poseen todos los directorios, la cual apunta al mismo directorio en cuestión, en este caso el inodo 2 que es este mismo.
\item[..] Esta es otra entrada estándar de tipo directorio (2) que poseen todos los directorios, apunta al directorio que contiene a este. Al ser el directorio raíz, este se apunta a sí mismo también, el número 2.
\item[Entrada Nula] La podemos detectar por el campo de inodo en 0. Podemos ignorarla en caso de lectura o usar el espacio vacío si quisiéramos escribir una entrada adicional en el directorio, siempre y cuando alcance el espacio.
\item[DeliriOS.bin] Este es un archivo, lo podemos detectar por el tipo (1). Está apuntando al inodo 23, si quisiéramos leerlo solo tenemos que cargar el inodo 23 y parsearlo correctamente. 
\item[var] Otro directorio (Tipo 2), ubicado en el inodo 30, si quisieramos leer su contenido tenemos que repetir el proceso actual pero con el inodo 30. Al ser la última entrada, necesariamente el tamaño abarca hasta el final del bloque. Para asegurarnos que llegamos al final, ya que podría haber más entradas en el bloque siguiente, chequeamos el tamaño del directorio (en este caso el raíz), si es 1024 entonces llegamos al final\footnote{Esto obliga a que el tamaño de los directorios sea siempre múltiplo del tamaño del bloque.}.
\end{description}

Dicho esto, ya tenemos una idea general de cómo se recorre EXT2, al menos para lectura.

\end{paragraph}

\end{subsubsection}

\end{subsection}

\pagebreak

\begin{subsection}{Implementación}

Como dijimos en secciones anteriores, DeliriOS posee una interfaz de funciones de filesystem que tenemos que cumplir para poder ser accedidos por las syscalls. \par

\begin{subsubsection}{FS Structs}
Una de las cosas importantes de esta interfaz es que tenemos que definir tres structs nosotros mismos, estos son:

\begin{paragraph}{fs\_info}
En este struct tenemos que definir los datos globales que queremos guardar del filesystem. En todas las funciones de la interfaz nos pasan un puntero a esta estructura. Definimos los siguientes campos:

\input{code/fs_info.tex}

Primero necesitamos dos cachés. Una de ellas la utilizaremos para acceder tanto al superbloque como a la tabla de grupos. La otra la usaremos para acceder al resto del filesystem a medida que se requiera. \par
La razón por la cual creamos una caché aparte para el superbloque y la tabla de grupos, es porque necesitamos que estos estén en memoria todo el tiempo. A la vez, necesitamos que la tabla de grupos no esté fragmentada entre bloques para poder accederla como un arreglo. Por lo tanto para aprovechar la funcionalidad de la caché, creamos una con tamaño de bloque que abarque desde el comienzo hasta la tabla y cargamos solo el primer bloque. \par
A partir de ahí podemos crear un puntero directo al superbloque y un puntero directo a la tabla que son los dos siguientes campos. \par
Luego vienen tres variables que son útiles, calculadas a partir de los datos del superbloque, son utilizadas según convenga en las distintas funciones:
\begin{description}
\item[block\_size] El tamaño en bytes del bloque.
\item[block\_shift] El logaritmo en base 2 de block\_size tal que hacer (n >> block\_shift) sea equivalente a (n / block\_size) para hacer la división más rápido.
\item[group\_count] La cantidad de grupos en el filesystem.
\end{description}

\end{paragraph}

\begin{paragraph}{fs\_file}
En este struct tenemos que definir los datos que queremos guardar de un archivo. En todas las funciones de la interfaz que manipulen un archivo nos pasan un puntero a esta estructura. \par
Un archivo clásico tiene que ser capaz de poder ser recorrido tanto con lecturas, escrituras o búsquedas, moviendo la posición del puntero acorde a cada acción dentro de los límites del archivo. \par
Para hacer eso definimos los siguientes campos:

\input{code/fs_file.tex}

De primera, se definen los siguientes campos:

\begin{description}
\item[inode] Puntero al inodo, cuando cargamos el archivo cargamos el bloque que contiene al inodo que lo representa y dejamos este campo apuntando a él. De esta forma podemos acceder a los datos del archivo constantemente.
\item[remaining] Cuántos bytes quedan hasta el final del archivo. Esto sería el tamaño del archivo menos la posición en bytes donde estamos ubicados dentro del archivo. Cuando se abre un archivo, esto tiene el tamaño del archivo, ya que empieza en la posición 0. A medida que se hagan operaciones sobre el archivo variará acorde.
\item[block\_number] El número de bloque absoluto de datos en el que estamos posicionados actualmente. Ni bien se cargue el archivo, este será el indicado por bn\_direct[0] en el inodo. Si se avanza lo suficiente como para caer en el siguiente bloque, este tendrá el valor de bn\_direct[1]. Si en vez de eso se avanza 12 bloques tendrá el valor del primer número del bloque indirecto simple apuntado por el inodo, y así sucesivamente.
\item[block\_index] El equivalente a la posición del archivo pero en bloques. Este es el número que le habíamos asignado a los bloques en la imagen de ejemplo. Arranca en 0 al abrirse, y vale exactamente (posición del archivo en bytes / block\_size) en cualquier otro momento.
\item[block\_offset] El offset en bytes dentro del actual bloque. Básicamente es el sobrante en bytes de block\_index. Se calcula como (posición del archivo en bytes \% block\_size). 
\end{description}

Es muy importante saber que, en todo momento, se da el siguiente invariante: \\
$block\_index * block\_size + block\_offset + remaining = file\_size$ \\
\indent Ya que nos deja en claro dónde estamos parados en el archivo exactamente. Luego se define:

\begin{description}
\item[single\_index] El equivalente a block\_index pero para los bloques indirectos simples, aunque partiendo desde 1. Si es un número distinto de 0, nos va a indicar qué bloque indirecto simple tenemos cargado en singly\_indirect, y lo podemos utilizar para comparar y saber si tenemos que cargar otro al movernos en el archivo. Esto es, vale 0 si no hay bloque simple cargado. Vale 1 si está cargado el bloque indirecto simple apuntado por el inodo, y así sucesivamente con el resto de los indirectos simples apuntados por bloques indirectos dobles. 
\item[singly\_indirect] Puntero al bloque indirecto simple cargado en memoria actualmente si es que hay uno, siempre es coherente con single\_index.
\item[double\_index] Lo mismo que single\_index pero para bloques dobles, esto es, vale 0 si no hay nada cargado, 1 si es el bloque indirecto doble apuntado por el inodo y 2 en adelante si es alguno de los que pertenecen al bloque indirecto triple.
\item[doubly\_indirect] Puntero al bloque doble indirecto cargado si es que hay uno. Siempre es coherente con double\_index.
\item[triply\_indirect] Puntero al bloque triple indirecto cargado si es que está cargado. Ya que hay solo uno, no es necesario un índice para identificarlo; o está cargado y es el del inodo, o no lo está.
\end{description}

Notar que tanto single\_index como double\_index no necesariamente son coherentes con block\_index en la implementación por temas de optimización. Si se avanzara en el archivo lo suficiente, y quedaran cargados los respectivos bloque doble y simple de esa posición, y luego se volviese al principio del archivo ni single\_index ni double\_index van a cambiar. Permanecerán cargados hasta que sea necesario reemplazarlos por otros. \par
Lo mismo pasa con el bloque indirecto triple, que una vez cargado permanecerá así hasta que se cierre el archivo. \par
Los otros campos son usados en ext2\_write que queda fuera del alcance de este trabajo.

\end{paragraph}

\begin{paragraph}{fs\_dir}
En este struct tenemos que definir los datos que queremos guardar de un directorio. En todas las funciones de la interfaz que manipulen un directorio nos pasan un puntero a esta estructura. Definimos los siguientes campos:

\input{code/fs_dir.tex}

Como un directorio es a la vez un archivo en EXT2, simplemente guardamos un archivo y usamos los datos del archivo-directorio correspondiente. A la vez, como en readdir tenemos que devolver un puntero a una entrada de directorio genérica POSIX nos vemos forzados a incluir una en el struct. \par
Nuevamente, los otros campos son usados en ext2\_write que queda fuera del alcance de este trabajo.

\end{paragraph}

\begin{paragraph}{Superbloque}

No tomamos todos los datos del superbloque, pero son relevantes:

\begin{description}
\item[ext2\_magic] Necesitamos que sea 0xEF53, si no, no es un EXT2 válido.
\item[fs\_state] Necesitamos que esté en CLEAN, que es cuando está en 1, si no, tiene inconsistencias y no podemos leerlo.
\item[req\_flags] Estos son los flags de features requeridos para poder interactuar con el filesystem. Si el driver de EXT2 no soporta alguno de estos features, no debería ser montado. Nosotros solo soportamos (y requerimos) el flag DIR\_TYPE, que como dijimos anteriormente toma la parte alta del tamaño del nombre de las entradas de directorio como el tipo de archivo. Por default, \textbf{mkfs} activa siempre dicho flag.
\item[req\_write\_flags] Estos son los flags de features requeridos para poder escribir en el filesystem. En este trabajo no le dimos soporte de escritura al driver así que no nos interesan por ahora.
\item[optional\_flags] Los flags de features opcionales del filesystem, los cuales son útiles para ganar performance si se acatasen. No es necesario tenerlos en cuenta (al menos en lectura). Por eso los ignoramos.
\item[bn\_superblock] El número de bloque donde está ubicado el superbloque, esto es 1 para bloques de 1KiB, o 0 para cualquier otro. Necesario para saber en qué bloque comienza la tabla de bloques.
\item[block\_size\_shift] Nos dice el tamaño de bloque como $1024 << block\_size\_shift bytes$. Lo utilizamos para calcular block\_size y block\_shift en fs\_info.
\item[number\_blocks y blocks\_per\_group] Cantidad de bloques en el filesystem y cantidad de bloques por grupo respectivamente. Necesario para calcular el group\_count en fs\_info, como $\lceil number\_blocks / blocks\_per\_group \rceil$.
\item[inode\_size e inodes\_per\_group] Tamaño de un inodo y cantidad de inodos por grupo respectivamente. Necesario para poder recorrer las tablas de inodos.
\end{description}

\end{paragraph}

\end{subsubsection}

\end{subsection}

\end{section}
